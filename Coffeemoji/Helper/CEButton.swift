import UIKit

extension UIButton {

    static func pickerButton(tag: Int = 0) -> UIButton {
        let button = UIButton(type: .system)
        button.tag = tag
        button.titleLabel?.font = UIFont.systemFont(ofSize: 48)
        button.imageView?.contentMode = .scaleAspectFill

        return button
    }

}