import UIKit

extension UIImage {

    func crop(to rect: CGRect) -> UIImage? {
        guard let cgImage = cgImage, let ref = cgImage.cropping(to: rect) else { return nil }
        return UIImage(cgImage: ref, scale: scale, orientation: imageOrientation)
    }

    func merge(to image: UIImage?, rect: CGRect) -> UIImage? {
        UIGraphicsBeginImageContext(size)
        draw(in: CGRect(origin: .zero, size: size))

        image?.draw(in: rect)

        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
