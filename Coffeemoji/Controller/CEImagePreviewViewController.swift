import UIKit

public class CEImagePreviewViewController: UIViewController {
    
    let image: UIImage
    let imageView = UIImageView()
    
    public init(image: UIImage) {
        self.image = image
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageView.frame = view.bounds
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        
        view.backgroundColor = .white
        view.addSubview(imageView)
    }
}
