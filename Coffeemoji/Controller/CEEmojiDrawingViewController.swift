import UIKit

class CEEmojiDrawingViewController: UIViewController {
    
    var interaction: CEDrawingView.Interaction {
        get {
            return emojiDrawingView.drawingView.interaction
        }
        set {
            emojiDrawingView.drawingView.interaction = newValue
            updateToolbarItems()
        }
    }
    
    override func loadView() {
        self.view = CEEmojiDrawingView()
    }

    var emojiDrawingView: CEEmojiDrawingView {
        return view as! CEEmojiDrawingView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emojiDrawingView.imagePicker.items = [.image(CEImages.Coffee1), .image(CEImages.Coffee2), .image(CEImages.Coffee3), .image(CEImages.Coffee4)]
        emojiDrawingView.emojiPicker.items = [.emoji("👌"), .emoji("👍"), .emoji("🎉")]
        
        emojiDrawingView.emojiPicker.selection = { [weak self] in self?.selectionAction(item: $0) }
        emojiDrawingView.imagePicker.selection = { [weak self] in self?.selectionAction(item: $0) }
        
        title = "EmojiDrawing"
        navigationController?.isToolbarHidden = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector(reset));
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(saveImage));
        updateToolbarItems()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        emojiDrawingView.drawingView.loadDefaultView()
    }

    private func selectionAction(item: CEPicker.CEItem) {
        switch item {
        case .emoji(let emoji):
            self.emojiDrawingView.drawingView.drawing.emoji.emojiString = emoji
        case .image(let image):
            self.emojiDrawingView.drawingView.drawing.background = image
            break
        }
    }
    
    private func updateToolbarItems() {
        let move = UIBarButtonItem(title: "Move", style: .plain, target: self, action: #selector(self.move))
        let spacing = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let erase = UIBarButtonItem(title: "Erase", style: .plain, target: self, action: #selector(self.erase))
        
        let unselectedTint: UIColor? = nil
        let selectedTint: UIColor = .gray
        move.tintColor = emojiDrawingView.drawingView.interaction == .move ? selectedTint : unselectedTint
        erase.tintColor = emojiDrawingView.drawingView.interaction == .erase ? selectedTint : unselectedTint
        
        toolbarItems = [move, spacing, erase]
    }
    
    // MARK: Actions
    
    @objc private func saveImage() {
        guard let croppedImage = emojiDrawingView.drawingView.drawing.background.crop(to: emojiDrawingView.drawingView.currentBackgroundRect), let mergedImage = croppedImage.merge(to: emojiDrawingView.drawingView.emojiImageView.image, rect: emojiDrawingView.drawingView.currentEmojiRect) else {
            //TODO - Show error message
            return
        }

        navigationController?.pushViewController(CEImagePreviewViewController(image: mergedImage), animated: true)
    }
    
    @objc private func move() {
        interaction = .move
    }
    
    @objc private func erase() {
        interaction = .erase;
    }
    
    @objc private func reset() {
        emojiDrawingView.drawingView.reset()
    }
}
