import UIKit
import SnapKit

class CEEmojiDrawingView: UIView {

    var imagePicker: CEPicker!
    var drawingView: CEDrawingView!
    var emojiPicker: CEPicker!

    init() {
        super.init(frame: .zero)

        backgroundColor = .white

        imagePicker = CEPicker()
        addSubview(imagePicker)

        drawingView = CEDrawingView()
        drawingView.backgroundColor = .clear
        addSubview(drawingView)

        emojiPicker = CEPicker()
        addSubview(emojiPicker)

        updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateConstraints() {
        imagePicker.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.left.right.equalTo(self)
            make.height.equalTo(SEEmojiDrawingSize.Height)
        }
        
        drawingView.snp.makeConstraints { make in
            make.top.equalTo(imagePicker.snp.bottom)
            make.left.right.equalTo(self)
            make.bottom.equalTo(emojiPicker.snp.top)
        }

        emojiPicker.snp.makeConstraints { make in
            make.left.right.equalTo(self)
            make.height.equalTo(SEEmojiDrawingSize.Height)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
        }
        
        super.updateConstraints()
    }

    struct SEEmojiDrawingSize {
        static let Height = 88
    }

}
