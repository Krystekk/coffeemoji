import UIKit
import SnapKit

public struct CEDrawing {
    
    public struct Emoji: Equatable {
        public var emojiString: String
        public var size: CGFloat

        public func drawEmoji(scale: CGFloat) -> UIImage? {
            let attributedString = NSAttributedString(string: emojiString, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: size / scale)])
            let emojiSize = attributedString.boundingRect(with: CGSize(width: CGFloat.infinity, height: CGFloat.infinity), options: [], context: nil).size
            
            let render = UIGraphicsImageRenderer(size: emojiSize)
            return render.image(actions: { rendererContext in
                attributedString.draw(in: CGRect(origin: .zero, size: emojiSize))
            })
        }

        public static func ==(lhs: Emoji, rhs: Emoji) -> Bool {
            return lhs.emojiString == rhs.emojiString && lhs.size == rhs.size
        }
    }
    
    public var size: CGSize {
        return background.size
    }
    public var background: UIImage
    public var emoji: Emoji

    public static let DefaultDrawing = CEDrawing(background: CEImages.Coffee1, emoji: CEDrawing.Emoji(emojiString: "👍", size: 312))
}

public class CEDrawingView: UIView, UIScrollViewDelegate {

    var scrollView: UIScrollView!
    var backgroundImageView: UIImageView!
    var emojiImageView: UIImageView!
    var indicatorView: UIView!
    private var canReset = false

    var currentBackgroundRect: CGRect {
        return CGRect(x: scrollView.contentOffset.x * imageViewScale, y: scrollView.contentOffset.y * imageViewScale, width: scrollView.bounds.width * imageViewScale, height: scrollView.bounds.height * imageViewScale)
    }

    var currentEmojiRect: CGRect {
        return CGRect(x: emojiImageView.frame.minX * imageViewScale, y: emojiImageView.frame.minY * imageViewScale, width: emojiImageView.bounds.width * imageViewScale, height: emojiImageView.bounds.height * imageViewScale)
    }

    private struct Stroke {
        let from: CGPoint
        let to: CGPoint
        let thickness: CGFloat
    }

    public enum Interaction {
        case move, erase
    }

    public var interaction: Interaction = .move {
        didSet {
            scrollView.isScrollEnabled = interaction == .move
        }
    }
    
    public var drawing: CEDrawing = CEDrawing.DefaultDrawing {
        didSet {
            if oldValue.background != drawing.background || canReset {
                setupBackground(background: drawing.background)
            }
            if oldValue.emoji != drawing.emoji || canReset {
                emojiImageView.image = drawing.emoji.drawEmoji(scale: imageViewScale)
            }
        }
    }

    private func setupBackground(background: UIImage) {
        backgroundImageView.image = drawing.background
        configureScrollView()
    }
    
    private func configureScrollView() {
        let scale = 1 / imageViewScale
        scrollView.minimumZoomScale = scale
        scrollView.maximumZoomScale = scale
        scrollView.setZoomScale(scale, animated: false)
        scrollView.setContentOffset(CGPoint(x: (drawing.background.size.width * scale - scrollView.bounds.width) / 2, y: (drawing.background.size.height * scale - scrollView.bounds.height) / 2), animated: false)
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)

        clipsToBounds = true
        
        scrollView = UIScrollView()
        scrollView.clipsToBounds = true
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        addSubview(scrollView)
        
        backgroundImageView = UIImageView()
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.isUserInteractionEnabled = true
        backgroundImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapBackground(gesture:))))
        scrollView.addSubview(backgroundImageView)
        
        emojiImageView = UIImageView()
        emojiImageView.isUserInteractionEnabled = true
        emojiImageView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(moveEmoji(gesture:))))
        addSubview(emojiImageView)

        indicatorView = UIView()
        indicatorView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.75)
        indicatorView.layer.cornerRadius = SEDrawingSize.IndicatorHeight / 2
        indicatorView.isUserInteractionEnabled = false
        indicatorView.isHidden = true
        addSubview(indicatorView)

        updateConstraints()
    }

    func reset() {
        emojiImageView.transform = .identity
        canReset = true
        drawing = CEDrawing.DefaultDrawing
        canReset = false
    }

    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return backgroundImageView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func updateConstraints() {
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        backgroundImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        emojiImageView.snp.makeConstraints { make in
            make.center.equalTo(scrollView)
        }

        indicatorView.snp.makeConstraints { make in
            make.left.top.equalTo(self)
            make.height.width.equalTo(SEDrawingSize.IndicatorHeight)
        }

        super.updateConstraints()
    }

    @objc private func moveEmoji(gesture: UIPanGestureRecognizer) {
        switch interaction {
        case .move:
            setEmojiPosition(for: gesture.location(in: self))
        case .erase:
            setErase(for: gesture)
        }
    }

    var lastPoint = CGPoint.zero
    private func setErase(for gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            setupIndicatorPosition(gesture)
            indicatorView.isHidden = false

            lastPoint = gesture.location(in: emojiImageView)
            lastPoint = CGPoint(x: lastPoint.x - indicatorView.frame.width, y: lastPoint.y - indicatorView.frame.height)
        case .changed:
            setupIndicatorPosition(gesture)
            lastPoint = setupErase(gesture)
        case .ended, .cancelled, .failed:
            setupIndicatorPosition(gesture)
            _ = setupErase(gesture)

            indicatorView.isHidden = true
        default:
            return
        }
    }

    private func setupIndicatorPosition(_ gesture: UIPanGestureRecognizer) {
        let indicatorPoint = gesture.location(in: self)
        indicatorView.transform = CGAffineTransform(translationX: indicatorPoint.x - indicatorView.frame.width / 2 - indicatorView.frame.width, y: indicatorPoint.y - indicatorView.frame.height / 2 - indicatorView.frame.height)
    }

    private func setupErase(_ gesture: UIPanGestureRecognizer) -> CGPoint  {
        var currentPoint = gesture.location(in: emojiImageView)
        currentPoint = CGPoint(x: currentPoint.x - indicatorView.frame.width, y: currentPoint.y - indicatorView.frame.height)
        drawLine(Stroke(from: lastPoint, to: currentPoint, thickness: SEDrawingSize.IndicatorHeight))

        return currentPoint
    }

    private func drawLine(_ stroke: Stroke) {
        UIGraphicsBeginImageContext(emojiImageView.bounds.size)
        let context = UIGraphicsGetCurrentContext()

        emojiImageView.image?.draw(in: emojiImageView.bounds)

        context?.move(to: stroke.from)
        context?.addLine(to: stroke.to)
        context?.setLineCap(.round)
        context?.setLineWidth(stroke.thickness)
        context?.setBlendMode(.destinationOut)
        context?.strokePath()

        emojiImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }

    @objc private func tapBackground(gesture: UITapGestureRecognizer) {
        if interaction == .move {
            setEmojiPosition(for: gesture.location(in: self))
        }
    }

    func loadDefaultView() {
        if backgroundImageView.frame.size == .zero && emojiImageView.frame.size == .zero {
            reset()
        }
    }
    
    private func setEmojiPosition(for point: CGPoint) {
        emojiImageView.transform = CGAffineTransform(translationX: point.x - (bounds.width / 2), y: point.y - (bounds.height / 2))
    }

    private var imageViewScale: CGFloat {
        return min(drawing.background.size.width / scrollView.bounds.width, drawing.background.size.height / scrollView.bounds.height)
    }

    struct SEDrawingSize {
        static let IndicatorHeight: CGFloat = 44
    }
}
