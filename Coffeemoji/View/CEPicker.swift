import UIKit
import SnapKit

public class CEPicker: UIView {
    
    public enum CEItem {
        case emoji(String)
        case image(UIImage)
    }
    
    public var selection : ((CEItem) -> ())?
    
    public var items: [CEItem] = [] {
        didSet {
            updateStackView()
        }
    }
    
    // MARK:
    
    private let buttons: [UIButton] = []
    private let stackView = UIStackView()
    
    // MARK:
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        stackView.alignment = .fill
        stackView.axis = .horizontal
        
        addSubview(stackView)

        updateStackView()
        updateConstraints()
    }

    public override func updateConstraints() {
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        super.updateConstraints()
    }

    private func updateStackView() {
        stackView.arrangedSubviews.forEach {
            stackView.removeArrangedSubview($0)
        }
        for (index, item) in items.enumerated() {
            prepareButton(for: index, item: item)
        }
    }

    private func prepareButton(for offset: Int, item: CEItem) {
        let button = UIButton.pickerButton(tag: offset)
        switch item {
        case .image(let image):
            button.setImage(image, for: .normal)
        case .emoji(let emoji):
            button.setTitle(emoji, for: .normal)
        }

        button.addTarget(self, action: #selector(selectItem(_:)), for: .touchUpInside)
        stackView.addArrangedSubview(button)
    }
    
    @objc private func selectItem(_ sender: UIButton) {
        selection?(items[sender.tag])
    }
    
}
