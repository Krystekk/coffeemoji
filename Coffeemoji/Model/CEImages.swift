import Foundation
import UIKit

public struct CEImages {
    public static let Coffee1 = bundeledJPEG(named: "Coffee1")!
    public static let Coffee2 = bundeledJPEG(named: "Coffee2")!
    public static let Coffee3 = bundeledJPEG(named: "Coffee3")!
    public static let Coffee4 = bundeledJPEG(named: "Coffee4")!
    
    private static func bundeledJPEG(named name: String) -> UIImage? {
        if let url = Bundle.main.path(forResource: name, ofType: "jpg") {
            return UIImage(contentsOfFile: url)?.withRenderingMode(.alwaysOriginal)
        } else {
            return nil
        }
    }
}
